"use strict";
//1 объект
let user = {
    name: "Ivan",
    adress: {
        city: "London",
        street: "Piccadilly"
    },
    lastName: "Grynko",
    a: ["a", "b", "c"]
};

//2 объект
let user2 = {
    fullName: {
        name: "Vasiliy",
        lastName: "Pupkin"
    },
    dateOfBirth: {
        year: "2000",
        day: 15,
        month: "july"
    },
    adress: {
        city: "Charkiv",
        state: "Ukraine"
    },
    car: "Volkswagen Touareg"
}

// 3 объект 

const user3 = {

    fullName: {
        name: {
            a: "Sveta",
            b: "Nastja"
        },
        lastName: {
            a: {
                1: "Makarova"
            },
            b: {
                1: "Ivanova",
                2: "Petrenko"
            }
        }
    },
    dateOfBirth: {
        day: 30,
        month: "may",
        year: 1987
    },

    children: {
        daughter: {
            fullName: {
                firstName: "Olga",
                lastName: "Klopotenko"
            },
            age: 9,
            dateOfBirth: {
                day: 1,
                month: "October",
                year: 2013
            },
            pupil: true

        },
        son: {
            fullName: {
                firstName: "Makar",
                lastName: "Klopotenko"
            },
            age: 1,
            dateOfBirth: {
                day: 11,
                month: "January",
                year: 2022
            },
            pupil: false
        }
    }
}


function deepCloneObj(obj) {
    if(!obj || typeof obj !== "object") {return obj};
    
    let cloneObj = {};
    for (let key in obj) {
        if (obj[key] instanceof Object) {
            cloneObj[key] = deepCloneObj(obj[key]);
            continue;
        }
        cloneObj[key] = obj[key];
    }
    return cloneObj;
}

let clone = deepCloneObj(user);
// let clone = deepCloneObj(user2)
// let clone = deepCloneObj(user3);


// 1-я проверка:
clone.name = "Petr";
clone.adress.city = "Lviv";
clone.adress.street = "Pynok";
clone.a[0] = "d";
console.log(clone);
console.log(user);

// 2-я проверка:
// clone.adress.city = "Poltava";
// clone.fullName.name = "Vita";
// clone.fullName.lastName = "Petuhova";
// clone.car = "Lada";
// console.log(user2);
// console.log(clone);

//3-я проверка:

// clone.children.daughter.fullName.firstName = "Lena";
// clone.children.daughter.age = 15;
// clone.dateOfBirth.year = 1970;
// clone.children.son.fullName.firstName = "Oleg";
// clone.children.daughter.pupil = false;
// console.log(user3);
// console.log(clone);